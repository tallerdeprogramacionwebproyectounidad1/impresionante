<?php
	require_once("lib/common.php");
	session_start();

	validarSesionTrabajador();

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php
			head();
		?>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand"><i class="fi-rr-scissors"></i> La Barbière de Paris <i class="fi-rr-scissors"></i></a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
   			<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="navbarText">
    			<ul class="navbar-nav mr-auto">
      			</ul>
    			<span class="navbar-text">
      				<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown" style="background-color: #000;">
						<?php echo $_SESSION['id_usu'];?> <span class="caret"></span></button>

						<ul class="dropdown-menu" style="background-color: #000" role="menu">
							<li><a  href="lib/logout.php"> > Cerrar Sesión   </a></li>
						</ul>
					</div>
      				<button class="btn btn-primary btn-lg" type="button" data-pushbar-target="pushbar-menu" style="background-color: #000"><i class="fas fa-bars"></i></button>
    			</span>
  			</div>
		</nav>
		<br>
		<main>
      		<nav>
        		<a class="banner" style="background-color: #fff">Bienvenido: <?php echo $_SESSION['id_usu'];?></a>
      		</nav>
    	</main>

		<div class="contenedor">
			<div data-pushbar-id="pushbar-menu" data-pushbar-direction="left" class="pushbar">
				<div class="btn-cerrar">
					<button data-pushbar-close><i class="fas fa-times"></i></button>
					<nav class="menu">
						<a> </a>
						<a href="#tab5" class="tab-text"> Gestionar Reservas Online <?php image(3) ?> </a>
						<a href="#tab4" class="tab-text"> servicio <?php image(3) ?> </a>
					</nav>
				</div>	
			</div>
		</div>
		<section class="secciones">
			<article id="tab2">
			</article>
			<article id="tab5">
        		<br><br> 
				<div class="tabla1"> 
            		<div class="col-lg-12">
                		<div class="card">
                    		<div id="titulo" class="card-header bg-secondary">
								Tabla de gestión de Reservas
                    		</div> 
							<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="id_table4" class="display table compact nowrap"></table>
                       			</div>
                    		</div> 
                		</div> 
            		</div>
        		</div>
			</article>
			<article id="tab4">
				<br><br> 
				<div class="tabla2"> 
            		<div class="col-lg-12">
                		<div class="card">
                   	 		<div class="card-header bg-secondary">
								Tabla de gestión de Noticias 
                    		</div> 

                    		<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="pene" class="display table compact nowrap"></table>
                       			</div>
                    		</div> 
                		</div> 
            		</div>
        		</div>
			</article>
		</section>

		<script src="controladores/javascript.js"></script>
		<script src="controladores/login.js"></script>
		<script src="bd/Reservas/Reservas.js"></script>
		<script src="bd/Reserva-usuario/Reservas-usuario.js"></script>

	</body>
	<!-- Redes sociales -->
	<script src="controladores/pushbar.js"></script>
	<script>
		var pushbar = new Pushbar({
			blur: true,
			overlay: true
		});
	</script>
</html>