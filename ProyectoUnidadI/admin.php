<?php
	require_once("lib/common.php");
	session_start();

	validarSesion();

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php
			head();
		?>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand"><i class="fi-rr-scissors"></i> La Barbière de Paris <i class="fi-rr-scissors"></i></a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
   			<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="navbarText">
    			<ul class="navbar-nav mr-auto">
      			</ul>
    			<span class="navbar-text">
      				<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown" style="background-color: #000;">
						<?php echo $_SESSION['id_usu'];?> <span class="caret"></span></button>

						<ul class="dropdown-menu" style="background-color: #000" role="menu">
							<li><a  href="lib/logout.php"> > Cerrar Sesión   </a></li>
						</ul>
					</div>
      				<button class="btn btn-primary btn-lg" type="button" data-pushbar-target="pushbar-menu" style="background-color: #000"><i class="fas fa-bars"></i></button>
    			</span>
  			</div>
		</nav>
		<br>
		<main>
      		<nav>
        		<a class="banner" style="background-color: #fff">Administración</a>
      		</nav>
    	</main>

		<div class="contenedor">
			<div data-pushbar-id="pushbar-menu" data-pushbar-direction="left" class="pushbar">
				<div class="btn-cerrar">
					<button data-pushbar-close><i class="fas fa-times"></i></button>
					<nav class="menu">
						<a> </a>
						<a href="#tab5" class="tab-text"> Gestionar Reservas Online <?php image(3) ?> </a>
						<a href="#tab6" class="tab-text"> Gestionar Noticias 		  <?php image(4) ?></a>
						<a href="#tab4" class="tab-text"> Gestionar Trabajadores    <?php image(4) ?></a>
						<a href="#tab8" class="tab-text"> Gestionar Servicios    <?php image(3) ?></a>
					</nav>
				</div>	
			</div>
		</div>

		<section class="secciones">
			<article id="tab2">
			</article>
			<article id="tab4">
				<div class="modal" id="modal-Trabajadores">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- header modal -->
							<div class="modal-header">
								<h4 class="modal-title"><span id="titulo-modal-Trabajadores">Crear</span></h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
				
							<!-- body modal-->
							<div class="modal-body">	
								<div class="row">
									<form id="form-Trabajadores" role="form" method="post" enctype="multipart/form-data" class="was-validated">	
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="password">Contraseña:</label>
											<input type="text" class="form-control" id="password" placeholder="Ingrese Contraseña" name="password" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
								
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="nombre_1">nombre:</label>
											<input type="text" class="form-control" id="nombre_1" placeholder="Ingrese nombre" name="nombre_1" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
								
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="dni">Rut:</label>
											<input type="text" class="form-control" id="dni" placeholder="Ingrese su Rut" name="dni" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="telefono_1">Telefono:</label>
											<input type="text" class="form-control" id="telefono_1" placeholder="Ingrese su Numero de Telefono" name="telefono_1" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
									</form>
									<div class="clearfix"></div>
								</div>
							</div>
							<!-- footer modal -->
							<div class="modal-footer">
								<button type="submit" name="btn-aceptar-Trabajadores" class="btn btn-secondary" id="btn-aceptar-Trabajadores" value="Subir">Aceptar</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
							</div>
						</div>
					</div>
				</div>
        		<br><br> 
				<div class="tabla1"> 
            		<div class="col-lg-12">
                		<div class="card">
                    		<div id="titulo" class="card-header bg-secondary">
								Tabla de gestión de Trabajadores <button type="button" class="btn btn-primary btn-lg" style="background-color: #000;"  data-toggle="modal" data-target="#miModal5">Estadisticas</button>
                    		</div> 

                    		<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="id_table5" class="display table compact nowrap"></table>
                       			</div>
                   	 		</div> 
                		</div> 
            		</div>
        		</div>
			</article>
			<article id="tab5">
        		<br><br> 
				<div class="tabla1"> 
            		<div class="col-lg-12">
                		<div class="card">
                    		<div id="titulo" class="card-header bg-secondary">
								Tabla de gestión de Reservas <button type="button" class="btn btn-primary btn-lg" style="background-color: #000;"  data-toggle="modal" data-target="#miModal6">Estadisticas</button>
                    		</div> 
							<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="id_table4" class="display table compact nowrap"></table>
                       			</div>
                    		</div> 
                		</div> 
            		</div>
        		</div>
			</article>
			<!-- Reservas -->
			<article id="tab8">
				<!-- x -->
				<div class="modal" id="modal-sucursal">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- header modal -->
							<div class="modal-header">
								<h4 class="modal-title"><span id="titulo-modal-sucursal">Crear</span></h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
				
							<!-- body modal-->
							<div class="modal-body">
				
								<div class="row">
									<form id="form-sucursal" role="form" method="post" enctype="multipart/form-data" class="was-validated">
		
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="nom_direccion">Nombre:</label>
											<input type="text" class="form-control" id="nom_direccion" placeholder="Ingrese Nombre" name="nom_direccion" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
								
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="telefono">Precio:</label>
											<input type="text" class="form-control" id="telefono" placeholder="Ingrese telefono" name="telefono" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="hora">Hora:</label>
											<input type="time" id="hora" name="hora">
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
									</form>
									<div class="clearfix"></div>
								</div>
							</div>
				
							<!-- footer modal -->
							<div class="modal-footer">
								<button type="submit" name="btn-aceptar-sucursal" class="btn btn-secondary" id="btn-aceptar-sucursal" value="Subir">Aceptar</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
							</div>
						</div>
					</div>
				</div>
        		<br><br> 
				<div class="tabla1"> 
            		<div class="col-lg-12">
                		<div class="card">
                    		<div id="titulo" class="card-header bg-secondary">
								Tabla de gestión de servicios 	<button type="button" class="btn btn-primary btn-lg" style="background-color: #000;"  data-toggle="modal" data-target="#miModal4">Estadisticas</button>
                    		</div> 

                    		<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="id_table2" class="display table compact nowrap"></table>
                       			</div>
                    		</div> 
                		</div> 
            		</div>
        		</div>
        		<br></br>
			</article>
			<!-- Noticias -->
			<article id="tab6">
				<!-- x -->
				<div class="modal" id="modal-Noticias">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- header modal -->
							<div class="modal-header">
								<h4 class="modal-title"><span id="titulo-modal-Noticias">Crear</span></h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
				
							<!-- body modal-->
							<div class="modal-body">
				
								<div class="row">
									<form id="form-Noticias" role="form" method="post" enctype="multipart/form-data" class="was-validated">
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="nombre">Nombre:</label>
											<input type="text" class="form-control" id="nombre" placeholder="Ingrese Nombre" name="nombre" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>

										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="descripcion">Descripcion:</label>
											<input type="text" class="form-control" id="descripcion" placeholder="Ingrese Nombre" name="descripcion" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
								
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="hora">Tiempo de expiracion:</label>
											<input type="time" id="hora_1" name="hora_!">
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
									</form>
									<div class="clearfix"></div>
								</div>
							</div>
				
							<!-- footer modal -->
							<div class="modal-footer">
								<button type="submit" name="btn-aceptar-Noticias" class="btn btn-secondary" id="btn-aceptar-Noticias" value="Subir">Aceptar</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
							</div>
						</div>
					</div>
				</div>
        		<br><br> 
				<div class="tabla2"> 
            		<div class="col-lg-12">
                		<div class="card">
                   	 		<div id="titulo" class="card-header bg-secondary">
								Tabla de gestión de Noticias 
                    		</div> 

                    		<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="id_table3" class="display table compact nowrap"></table>
                       			</div>
                    		</div> 
                		</div> 
            		</div>
        		</div>
			</article>
			<!-- Estadistica -->
			<article id="tab7">
			</article>
		</section>
		<div class="modal fade" id="miModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		 	<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
			    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
		        		</button>
		    			<!--<h4 class="modal-title" id="myModalLabel">Esto es un modal</h4> -->
					</div>
					<div class="modal-body">
						<!-- Hay que ver como cambiar el parametro -->
						<div class="row">
							<div class ="col-md-12" id="contenedor">
								<canvas id="miGrafico"></canvas>
							</div>
						</div>
					</div>
		    	</div>
		  	</div>
		</div>
		<div class="modal fade" id="miModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		 	<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
			    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
		        		</button>
		    			<!--<h4 class="modal-title" id="myModalLabel">Esto es un modal</h4> -->
					</div>
					<div class="modal-body">
						<!-- Hay que ver como cambiar el parametro -->
						<div class="row">
							<div class ="col-md-12" id="contenedor">
								<canvas id="miGrafico_2"></canvas>
							</div>
						</div>
					</div>
		    	</div>
		  	</div>
		</div>

		<div class="modal fade" id="miModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		 	<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
			    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
		        		</button>
		    			<!--<h4 class="modal-title" id="myModalLabel">Esto es un modal</h4> -->
					</div>
					<div class="modal-body">
						<!-- Hay que ver como cambiar el parametro -->
						<div class="row">
							<div class ="col-md-12" id="contenedor">
								<canvas id="miGrafico_3"></canvas>
							</div>
						</div>
					</div>
		    	</div>
		  	</div>
		</div>

		<script src="controladores/javascript.js"></script>
		<script src="controladores/login.js"></script>
		<script src="bd/Noticias/Noticias.js"></script>
		<script src="bd/Servicios/Servicios.js"></script>
		<script src="bd/Reservas/Reservas.js"></script>
		<script src="bd/Trabajadores/Trabajadores.js"></script>
		<script src="bd/Grafico.js"></script>
		<script src="bd/Grafico_2/Grafico_2.js"></script>
		<script src="bd/Grafico_3/Grafico_3.js"></script>									
	</body>
	<!-- Redes sociales -->
	<script src="controladores/pushbar.js"></script>
	<script>
		var pushbar = new Pushbar({
			blur: true,
			overlay: true
		});
	</script>
</html>