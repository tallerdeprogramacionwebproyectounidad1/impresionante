<?php
	require_once("lib/common.php");
	session_start();
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php
			head();
		?>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand"><i class="fi-rr-scissors"></i> La Barbière de Paris <i class="fi-rr-scissors"></i></a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
   			<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="navbarText">
    			<ul class="navbar-nav mr-auto">
      			</ul>
    			<span class="navbar-text">
      				<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown" style="background-color: #000;">
						<?php echo $_SESSION['id_usu'];?> <span class="caret"></span></button>

						<ul class="dropdown-menu" style="background-color: #000" role="menu">
							<li><a  href="lib/logout.php"> > Volver al menú </a></li>
						</ul>
					</div>
    			</span>
  			</div>
		</nav>
		<div class="modal" id="modal-reserva-usuario">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- header modal -->
							<div class="modal-header">
								<h4 class="modal-title"><span id="titulo-modal-reserva-usuario">Crear</span></h4>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
				
							<!-- body modal-->
							<div class="modal-body">	
								<div class="row">
									<form id="form-reserva-usuario" role="form" method="post" enctype="multipart/form-data" class="was-validated">	
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="rut">Rut:</label>
											<input type="text" class="form-control" id="rut" placeholder="Ingrese Contraseña" name="rut" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<label for="cars">Choose a car:</label>
												<select name="cars" id="cars">
												<script>
		       										$.ajax({
		    											url: "bd/Reservas.php",
													    data: {accion: 1},
														type: 'POST',
														dataType: 'json',
														async: true,
														success: function(response) {
		    												if(response.success){
		    													var data = response.data;
		    													var nombre = [];
		    													var dni = [];
		    													var select = document.getElementById('cars');
		    													var frag = document.createDocumentFragment();
		    													for (var i = 0; i < data.length; i++) {
		    														nombre.push(data[i]["dni"]);
		    														dni.push(data[i]["nombre"]);
		    														var opt = document.createElement('option');
		    														opt.value = nombre;
		    														opt.innerHTML = dni;
		    														frag.appendChild(opt);
		    													}
		    													select.appendChild(frag);
		    												}
		    											},
		    										});
		    									</script>
		  									</select>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="time" id="hora" name="hora">	 			
											<input type="date" id="calendario" name="calendario">
										</div>
									</form>
									<div class="clearfix"></div>
								</div>
							</div>
							<!-- footer modal -->
							<div class="modal-footer">
								<button type="submit" name="btn-aceptar-reserva-usuario" class="btn btn-secondary" id="btn-aceptar-reserva-usuario" value="Subir">Aceptar</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
							</div>
						</div>
					</div>
				</div>
		<br>
		<main>
      		<nav>
        		<a class="banner" style="background-color: #fff">Bienvenido: <?php echo $_SESSION['id_usu'];?></a>
      		</nav>
    	</main>
		<br><br> 
		<div class="tabla2"> 
            <div class="col-lg-12">
                <div class="card">
                   	<div class="card-header bg-secondary">
						Tabla de Reservas 
                    		</div> 

                    		<div class="card-body">
                        		<div class="table-responsive">
                            		<table id="pene" class="display table compact nowrap"></table>
                       			</div>
                    		</div> 
                		</div> 
            		</div>
        		</div>

		<script src="controladores/login.js"></script>
		<script src="bd/Reserva-usuario/Reservas-usuario.js"></script>

	</body>
	<!-- Redes sociales -->
	<script src="controladores/pushbar.js"></script>
	<script>
		var pushbar = new Pushbar({
			blur: true,
			overlay: true
		});
	</script>
</html>