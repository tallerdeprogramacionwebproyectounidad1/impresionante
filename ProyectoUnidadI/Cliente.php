<?php
	require_once("lib/common.php");
	session_start();
	validarSesionCliente();
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<?php
			head();
		?>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand"><i class="fi-rr-scissors"></i> La Barbière de Paris <i class="fi-rr-scissors"></i></a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
   			<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="navbarText">
    			<ul class="navbar-nav mr-auto">
      			</ul>
    			<span class="navbar-text">
      				<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle btn-lg" data-toggle="dropdown" style="background-color: #000;">
						<?php echo $_SESSION['id_usu'];?> <span class="caret"></span></button>

						<ul class="dropdown-menu" style="background-color: #000" role="menu">
							<li><a  href="lib/logout.php"> > Cerrar Sesión   </a></li>
						</ul>
					</div>
      				<button class="btn btn-primary btn-lg" type="button" data-pushbar-target="pushbar-menu" style="background-color: #000"><i class="fas fa-bars"></i></button>
    			</span>
  			</div>
		</nav>
		<div class="contenedor">
			<header>
				<?php
					logo();
				?>
				<div data-pushbar-id="pushbar-menu" data-pushbar-direction="left" class="pushbar">
					<div class="btn-cerrar">
						<button data-pushbar-close><i class="fas fa-times"></i></button>
							<nav class="menu">
								<a href="#tab3" class="tab-text"> Inicio  		  <?php image(1) ?></a>
								<a href="#tab4" class="tab-text"> Contacto 		  <?php image(2) ?> </a>
								<a href="#tab5" class="tab-text"> Reservas Online <?php image(3) ?> </a>
								<a href="#tab6" class="tab-text"> Noticias 		  <?php image(4) ?></a>
							</nav>
						</div>	
					</div>
				</div>
			</header>
		</div>
		<section class="secciones">
			<!-- Reservas -->
			<article id="tab1">
				<button class="enlace" role="link" onclick="window.location='reserva_online.php'">Reserve su Hora</button>
			</article>
			<article id="tab5">
			</article>
			<!-- Noticias -->
			<article id="tab6">
				<?php
					Noticias();
				?>
			</article>
			<!-- contacto -->
			<article id="tab4">
				<?php
					Contacto();
				?>
			</article>
			<!-- Inicio -->
			<article id="tab3">
				<?php
					Inicio();
				?>
			</article>
			<!-- Estadistica -->
		</section>
		<script src="controladores/javascript.js"></script>
		<script src="controladores/login.js"></script>			
	</body>
	<!-- Redes sociales -->
	<footer class="contenedor">
		<?php
			footer();
		?>		
	</footer>
    <!-- Redes sociales -->
	<script src="https://unpkg.com/web-animations-js@2.3.2/web-animations.min.js"></script>
	<script src="controladores/pushbar.js"></script>
	<script>
		var pushbar = new Pushbar({
			blur: true,
			overlay: true
		});
	</script>
</html>