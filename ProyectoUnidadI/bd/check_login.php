<?php
session_start();
require('conexionBd.php');

$_SESSION['loggedin'] = false;
$_SESSION['id_usu'] = null;
$_SESSION['nemp'] = 0;
$success = false;
$msg = "Error de usuario o clave";
$location = "../lib/logout.php";
$_SESSION['loggedinMIAPP'] = false;
$_SESSION['loggedintrabajador'] = false;
$_SESSION['loggedinusuario'] = false;
$_SESSION['last_activity'] = 0;

$conn = conectarBD();

if ($conn) {
  if (isset($_POST['correo'], $_POST['clave'])) {
    if ($_POST['correo']!="" and $_POST['clave']!="") {
      $id_usu = $_POST['correo'];
      $password = $_POST['clave'];
            
      $sql = "select dni, id_perfil from usuario where dni = :id_usu and password = :password";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(':id_usu', $id_usu);
      $stmt->bindValue(':password', $password);

      if ($stmt->execute()) {
        $array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (count($array_session) == 1) {
          $_SESSION['id_usu'] = $id_usu;
          $id = $array_session[0]["id_perfil"];
          $success = true;
          $msg = "ok";
          if($id == 0){
            $_SESSION['loggedinMIAPP'] = true;
           $location = "./admin.php";
          }
          if($id == 1){
            $_SESSION['loggedinusuario'] = true;
            $location = "./Cliente.php";
          }
          if($id == 2){
            $_SESSION['loggedintrabajador'] = true;
            $location = "./Trabajador.php";
          }

        } else {
          $msg = "Usuario o Clave erróneos.";
        }
      } else {
        $msg = "Error al ejecutar la consulta.";
      }
    } else {
      $msg = "Todos los datos son requeridos.";
    }
  } else {
    $msg = "Todos los datos son requeridos.";
  }
} else {
  $msg = "No puede conectar a la Base de Datos.";
}

$jsonOutput = array('success' => $success, 'msg' => $msg, 'location'=> $location);
echo json_encode($jsonOutput);
?>
