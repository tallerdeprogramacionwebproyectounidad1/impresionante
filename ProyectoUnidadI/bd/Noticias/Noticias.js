$(function() { 
    obtener_Noticias();   
});

function obtener_Noticias() {
	
	var accion_agregar_noticias = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_noticias();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";        
    
	
	var tabla = $('#id_table3').dataTable({
		"columnDefs": [
          {"title": "Id_noticias", "targets": 0, "orderable": true, "className": "dt-body-center"},
 		  {"title": "Nombre", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Descripcion", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Expiracion", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar_noticias, "targets": 4, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "./language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	   $.ajax({
		url: 'bd/Noticias/Noticias.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.success) {
				var data = response.data;
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
						data[i]["id_aviso"],
					    data[i]["nombre"],
					    data[i]["descripcion"],
					    data[i]["expiracion"],

					    "<button type='button' class='btn btn-warning btn-xs' onclick='editar_1(" + data[i]["id_aviso"] + ");' title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
					
					    "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar_1(" + data[i]["id_aviso"] + ");' title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"

				    ]);
                }   
			}       
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function agregar_noticias() {
    $("#titulo-modal-Noticias").html("Agregar Noticias"); 
    document.getElementById("form-Noticias").reset();
    $("#btn-aceptar-Noticias").attr("onClick", "agregar_noticiasBD()");
    $("#modal-Noticias").modal("show");
}

function agregar_noticiasBD() {
    val_nombre = validarFormulario('#nombre', "nombre");
    val_expirar = validarFormulario('#hora_1', "hora_1");

	if (val_nombre == false || val_expirar == false){
         return false;
    }

	var nombre = $('#nombre').val();
	var expirar = $('#hora_1').val();
	var descripcion = $('#descripcion').val();
    
    $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Noticias/Noticias.php?accion=2',
		data: { nombre: nombre,
                expirar: expirar,
                descripcion: descripcion},

		success: function (response) {    
		    if (response.success) {          
			    $("#modal-Noticias").modal("hide");
			    obtener_Noticias();
			  
		    }

		},
    });
}

function validarFormulario (id, nombre) {
    if ($(id).val().trim().length<1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}

function eliminar_1(x){
	 $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Noticias/Noticias.php?accion=3',
		data: {x: x},


		success: function (response) {     
		    if (response.success) {
		    	obtener_Noticias();  
		    }
		},
    });
}

function editar_1(id_aviso) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: 'bd/Noticias/Noticias.php?accion=4',
        data: {id_noticias: id_aviso},
        success: function (response) {
        	console.log(response.success);  
            if (response.success) {
                $.each(response.data, function(index, value) {
                    $('#descripcion').val(response.data["descripcion"]);
                    $('#nombre').val(response.data["nombre"]);
                    $('#hora_1').val(response.data["expiracion"]);
                });
    
                $("#titulo-modal-Noticias").html("Editar");
                $("#btn-aceptar-Noticias").attr('onclick','editar_1BD('+id_aviso+');');
                $("#modal-Noticias").modal("show");
            }   
        },
    }); 
}

function editar_1BD(id_aviso) {
    var descripcion = $('#descripcion').val();
	var nombre = $('#nombre').val();
	var expirar = $('#hora_1').val();
    

    $.ajax({
        dataType: 'json',
        async: true,
        url: 'bd/Noticias/Noticias.php?accion=5',
		data: {id_aviso: id_aviso,
			   nombre: nombre, 
               expirar: expirar,
               descripcion: descripcion},
                    
        success: function (response) {
        	console.log(response.success);
            if (response.success) {          
                $("#modal-Noticias").modal("hide");
                obtener_Noticias();
            } 
        }, 
    });
}
