<?php

require_once("../conexionBd.php");

session_start();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar_1($conn);
      break;
    case 2:
      # insertar
      insertar($conn);
      break;
    case 3:
      # delete
      eliminar($conn);
      break;
    case 4:
      # update
      seleccionarUno($conn);
      break;
    case 5:
      # update
      actualizar($conn);
      break;         
  }  
}

function seleccionar_1($conn) {
  $sql= "select * from avisos;";
  
  $stmt = $conn->prepare($sql);
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function insertar($conn) {
  $descripcion = trim($_REQUEST['descripcion']);
  $nombre = trim($_REQUEST['nombre']);
  $expirar = trim($_REQUEST['expirar']);

  $sql = "insert into avisos(nombre, expiracion, descripcion) values (:nombre, :expirar, :descripcion);";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':nombre', $nombre);
  $stmt->bindValue(':expirar', $expirar);
  $stmt->bindValue(':descripcion', $descripcion);

  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function eliminar($conn) {
  $id_aviso = $_REQUEST['x'];

  $sql = "delete from avisos where id_aviso = :id_aviso;";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_aviso', $id_aviso);
  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function actualizar ($conn) {
  $id_aviso = trim($_REQUEST['id_aviso']);
  $nombre = trim($_REQUEST['nombre']);
  $expirar = trim($_REQUEST['expirar']);
  $descripcion = trim($_REQUEST['descripcion']);
 
  $sql = "update avisos set nombre = :nombre, expiracion = :expirar, descripcion = :descripcion where id_aviso = :id_aviso;";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_aviso', $id_aviso);
  $stmt->bindValue(':nombre', $nombre);
  $stmt->bindValue(':expirar', $expirar);
  $stmt->bindValue(':descripcion', $descripcion);
  
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function seleccionarUno ($conn) {
  $id_aviso = $_REQUEST['id_noticias'];
  
  $sql = "select * from avisos where id_aviso = :id_aviso;";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_aviso', $id_aviso);

  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

?>