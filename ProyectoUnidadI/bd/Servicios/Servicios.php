<?php

require_once("../conexionBd.php");

session_start();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar_1($conn);
      break;
    case 2:
      # insertar
      insertar($conn);
      break;
    case 3:
      # delete
      eliminar($conn);
      break;
    case 4:
      # update
      seleccionarUno($conn);
      break;
    case 5:
      # update
      actualizar($conn);
      break;         
  }  
}

function seleccionar_1 ($conn) {
  $sql= "select * from servicio;";
  
  $stmt = $conn->prepare($sql);
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function insertar($conn) {
  $nombre = trim($_REQUEST['direccion']);
  $precio = trim($_REQUEST['telefono']);
  $tiempo = trim($_REQUEST['tiempo']);

  $sql = "insert into servicio(tiempo, precio, nombre) values (:tiempo, :precio, :nombre);";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':precio', $precio);
  $stmt->bindValue(':nombre', $nombre);
  $stmt->bindValue(':tiempo', $tiempo);

  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function eliminar($conn) {
  $id_servicio = $_REQUEST['x'];

  $sql = "delete from servicio where id_servicio = :id_servicio;";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_servicio', $id_servicio);
  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function actualizar ($conn) {
  $id_servicio = trim($_REQUEST['id_servicio']);
  $nombre = trim($_REQUEST['direccion']);
  $precio = trim($_REQUEST['telefono']);
  $tiempo = trim($_REQUEST['hora']);
 

  $sql = "update servicio set nombre = :nombre, precio = :precio, tiempo = :tiempo where id_servicio = :id_servicio;";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':tiempo', $tiempo);
  $stmt->bindValue(':nombre', $nombre);
  $stmt->bindValue(':precio', $precio);
  $stmt->bindValue(':id_servicio', $id_servicio);
  
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function seleccionarUno ($conn) {
  $id_servicio = $_REQUEST['id_servicio'];
  
  $sql = "select tiempo, precio, nombre from servicio where id_servicio = :id_servicio;";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_servicio', $id_servicio);

  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

?>