$(function() { 
    obtener_sucursales();   
});

function obtener_sucursales() {
	
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_sucursal();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";        
    
	
	var tabla = $('#id_table2').dataTable({
		"columnDefs": [
          {"title": "Id_servicio", "targets": 0, "orderable": true, "className": "dt-body-center"},
 		  {"title": "Nombre", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Precio", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Tiempo", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 4, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "./language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	   $.ajax({
		url: 'bd/Servicios/Servicios.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.success) {
				var data = response.data;
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
						data[i]["id_servicio"],
					    data[i]["nombre"],
					    data[i]["precio"],
					    data[i]["tiempo"],

					    "<button type='button' class='btn btn-warning btn-xs' onclick='editar(" + data[i]["id_servicio"] + ");' title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
					
					    "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar(" + data[i]["id_servicio"] + ");' title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"

				    ]);
                }   
			}       
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function agregar_sucursal() {
    $("#titulo-modal-sucursal").html("Agregar sucursal"); 
    document.getElementById("form-sucursal").reset();
    $("#btn-aceptar-sucursal").attr("onClick", "agregar_sucursalBD()");
    $("#modal-sucursal").modal("show");
}

function agregar_sucursalBD() {
    val_telefono = validarFormulario('#telefono', "teléfono");
    val_direccion = validarFormulario('#nom_direccion', "direccion");

	if (val_telefono == false || val_direccion == false){
         return false;
    }

    var direccion = $('#nom_direccion').val();
	var telefono = $('#telefono').val();
	var tiempo = $('#hora').val();
    
    $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Servicios/Servicios.php?accion=2',
		data: {direccion: direccion,
                telefono: telefono,
            	tiempo: tiempo},

		success: function (response) {    
		    if (response.success) {          
			    $("#modal-sucursal").modal("hide");
			    obtener_sucursales();
			  
		    }

		},
    });
}

function validarFormulario (id, nombre) {
    if ($(id).val().trim().length<1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}

function eliminar(x){
	 $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Servicios/Servicios.php?accion=3',
		data: {x: x},


		success: function (response) {     
		    if (response.success) {
		    	obtener_sucursales();  
		    }
		},
    });
}

function editar(id_servicio) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: 'bd/Servicios/Servicios.php?accion=4',
        data: {id_servicio: id_servicio},
        success: function (response) {    
            if (response.success) {
                $.each(response.data, function(index, value) {
                    $("#nom_direccion").val(response.data["nombre"]);
                    $('#telefono').val(response.data["precio"]);
                    $('#hora').val(response.data["tiempo"]);
                });
    
                $("#titulo-modal-sucursal").html("Editar");
                $("#btn-aceptar-sucursal").attr('onclick','editarBD('+id_servicio+');');
                $("#modal-sucursal").modal("show");
            }   
        },
    }); 
}

function editarBD(id_servicio) {
    var direccion = $('#nom_direccion').val();
	var telefono = $('#telefono').val();
	var hora = $('#hora').val();
    

    $.ajax({
        dataType: 'json',
        async: true,
        url: 'bd/Servicios/Servicios.php?accion=5',
		data: {id_servicio: id_servicio,
			   direccion: direccion, 
               telefono: telefono, 
               hora: hora},
                    
        success: function (response) {
            if (response.success) {          
                $("#modal-sucursal").modal("hide");
                obtener_sucursales();
            } 
        }, 
    });
}



