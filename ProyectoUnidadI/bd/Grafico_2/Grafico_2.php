<?php

require_once("../conexionBd.php");

session_start();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar_1($conn);
      break;
  }
}

function seleccionar_1($conn) {
  $sql= "select (select count(*) from usuario where id_perfil = 1) as Cliente, (select count(*) from usuario where id_perfil = 2) as Trabajador;";
  
  $stmt = $conn->prepare($sql);
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

?>