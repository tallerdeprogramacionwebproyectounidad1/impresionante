$(function() { 
    obtener_Reserva_usuario();   
});

function obtener_Reserva_usuario() {
	
	var tabla = $('#pene').dataTable({
		"columnDefs": [
          {"title": "Id_servicio", "targets": 0, "orderable": true, "className": "dt-body-center"},
 		  {"title": "Nombre", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "precio", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "tiempo", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": "Boton", "targets": 4, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "./language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	   $.ajax({
		url: 'bd/Reserva-usuario/Reservas_usuario.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.success) {

				var data = response.data;
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
						data[i]["id_servicio"],
					    data[i]["nombre"],
					    data[i]["precio"],
					    data[i]["tiempo"],

					    "<button type='button' class='btn btn-warning btn-xs' onclick='agregar_trabajadores(" + data[i]["id_servicio"] + ");' title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;"
					

				    ]);
                }   
			}       
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function agregar_trabajadores(id_servicio) {
    $("#titulo-modal-reserva-usuario").html("Agregar Reservas de Usuario"); 
    document.getElementById("form-reserva-usuario").reset();
    $("#btn-aceptar-reserva-usuario").attr("onClick", "agregar_TrabajadoresBD("+id_servicio+")");
    $("#modal-reserva-usuario").modal("show");
}

function agregar_TrabajadoresBD(id_servicio) {
    val_rut = validarFormulario('#rut', "rut");
    val_usurut = validarFormulario('#cars', "cars");
    val_hora = validarFormulario('#hora', "hora");
    val_calendario = validarFormulario('#calendario', "calendario");


	if (val_rut == false || val_usurut == false || val_hora == false || val_calendario == false){
         return false;
    }

    var rut = $('#rut').val();
	var usurut = $('#cars').val();
	var hora = $('#hora').val();
	var calendario = $('#calendario').val();
	var resultado = calendario+" "+hora;

    $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Reserva-usuario/Reservas_usuario.php?accion=2',
		data: {id_servicio: id_servicio,
                rut: rut,
                usurut: usurut,
            	resultado: resultado},

		success: function (response) {
		console.log(response.data); 
		    if (response.success) {          
			    $("#modal-reserva-usuario").modal("hide");
			    obtener_Reserva_usuario();
			  
		    }

		},
    });
}


function validarFormulario (id, nombre) {
    if ($(id).val().trim().length<1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}