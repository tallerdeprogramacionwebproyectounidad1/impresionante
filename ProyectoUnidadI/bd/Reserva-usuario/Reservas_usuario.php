<?php

require_once("../conexionBd.php");

session_start();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar_1($conn);
      break;
    case 2:
      # insertar
      insertar($conn);
      break;       
  }  
}

function seleccionar_1 ($conn) {
  $sql= "select * from servicio;";
  
  $stmt = $conn->prepare($sql);
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function insertar($conn) {
  $rut = trim($_REQUEST['rut']);
  $usurut = trim($_REQUEST['usurut']);
  $resultado = trim($_REQUEST['resultado']);
  $id_servicio = trim($_REQUEST['id_servicio']);

  $sql = "insert into reserva(dni, id_servicio, usu_dni, horafecha) values (:rut, :id_servicio, :usurut, :resultado);";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':rut', $rut);
  $stmt->bindValue(':usurut', $usurut);
  $stmt->bindValue(':id_servicio', $id_servicio);
  $stmt->bindValue(':resultado', $resultado);

  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}