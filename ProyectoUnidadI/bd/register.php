<?php

require('conexionBd.php');

$_SESSION['loggedin'] = false;
$_SESSION['id_usu'] = null;
$_SESSION['nemp'] = 0;
$success = false;
$msg = "Error de usuario o clave";
$_SESSION['loggedinMIAPP'] = false;	
$_SESSION['last_activity'] = 0;

$conn = conectarBD();

if ($conn) {
	if (isset($_POST['nombre'], $_POST['rut'], $_POST['telefono'], $_POST['clave1'], $_POST['clave2'])) {
    	if ($_POST['nombre']!="" and $_POST['rut']!="" and $_POST['telefono']!="" and $_POST['clave1']!="" and $_POST['clave2']!="") {
      		$nombre = $_POST['nombre'];
      		$pwd = $_POST['clave1'];
      		$rut = $_POST['rut'];
     	 	$telefono = $_POST['telefono'];
      		$pwd2 = $_POST['clave2'];
      		
      		if ($pwd == $pwd2){
	    		$sql = "insert into usuario (password, nombre, dni, id_perfil, telefono) values (:pwd, :nombre, :rut, 1, :telefono)";
	    		$stmt = $conn->prepare($sql);
	    		$stmt->bindValue(':nombre', $nombre);
	    		$stmt->bindValue(':pwd', $pwd);
	    		$stmt->bindValue(':rut', $rut);
	    		$stmt->bindValue(':telefono', $telefono);
	    		$success = true;
	    		$location = "./uwu.php";
		    	if ($stmt->execute()) {
	        		$array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);

	    			}
    		}else{
        		$msg = "Las claves no coinciden";
    		}
  		}
  		else{
  			$msg = "Todos los datos son requeridos.";
  		}
	}
	else{
		$msg = "No puede conectar a la Base de Datos.";
	}
}
else{
	$msg = "no se";
}	

$jsonOutput = array('success' => $success, 'msg' => $msg, 'location'=> $location);
echo json_encode($jsonOutput);

?>