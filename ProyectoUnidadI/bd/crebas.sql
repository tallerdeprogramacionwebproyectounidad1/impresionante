/*==============================================================*/
/* DBMS name:      PostgreSQL 7.3                               */
/* Created on:     28-06-2021 19:11:52                          */
/*==============================================================*/


drop index AVISOS_PK;

drop table AVISOS;

drop index PERFIL_PK;

drop table PERFIL;

drop index SE_FK;

drop index HACE_FK;

drop index OFRECE_FK;

drop index RESERVA_PK;

drop table RESERVA;

drop index SERVICIO_PK;

drop table SERVICIO;

drop index POSEE_FK;

drop index USUARIO_PK;

drop table USUARIO;

/*==============================================================*/
/* Table: AVISOS                                                */
/*==============================================================*/
create table AVISOS (
ID_AVISO             SERIAL               not null,
NOMBRE               VARCHAR(30)          not null,
EXPIRACION           TIME                 not null,
DESCRIPCION          VARCHAR(1024)        not null,
constraint PK_AVISOS primary key (ID_AVISO)
);

/*==============================================================*/
/* Index: AVISOS_PK                                             */
/*==============================================================*/
create unique index AVISOS_PK on AVISOS (
ID_AVISO
);

/*==============================================================*/
/* Table: PERFIL                                                */
/*==============================================================*/
create table PERFIL (
ID_PERFIL            INT4                 not null,
NOMBRE               VARCHAR(30)          not null,
constraint PK_PERFIL primary key (ID_PERFIL)
);

/*==============================================================*/
/* Index: PERFIL_PK                                             */
/*==============================================================*/
create unique index PERFIL_PK on PERFIL (
ID_PERFIL
);

/*==============================================================*/
/* Table: RESERVA                                               */
/*==============================================================*/
create table RESERVA (
ID_RESERVA           SERIAL               not null,
DNI                  VARCHAR(30)          not null,
ID_SERVICIO          INT4                 not null,
USU_DNI              VARCHAR(30)          not null,
HORAFECHA            DATE                 not null,
constraint PK_RESERVA primary key (ID_RESERVA)
);

/*==============================================================*/
/* Index: RESERVA_PK                                            */
/*==============================================================*/
create unique index RESERVA_PK on RESERVA (
ID_RESERVA
);

/*==============================================================*/
/* Index: OFRECE_FK                                             */
/*==============================================================*/
create  index OFRECE_FK on RESERVA (
ID_SERVICIO
);

/*==============================================================*/
/* Index: HACE_FK                                               */
/*==============================================================*/
create  index HACE_FK on RESERVA (
USU_DNI
);

/*==============================================================*/
/* Index: SE_FK                                                 */
/*==============================================================*/
create  index SE_FK on RESERVA (
DNI
);

/*==============================================================*/
/* Table: SERVICIO                                              */
/*==============================================================*/
create table SERVICIO (
ID_SERVICIO          SERIAL               not null,
TIEMPO               TIME                 not null,
PRECIO               FLOAT8               not null,
NOMBRE               VARCHAR(30)          null,
constraint PK_SERVICIO primary key (ID_SERVICIO)
);

/*==============================================================*/
/* Index: SERVICIO_PK                                           */
/*==============================================================*/
create unique index SERVICIO_PK on SERVICIO (
ID_SERVICIO
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
PASSWORD             VARCHAR(512)         not null,
NOMBRE               VARCHAR(30)          not null,
DNI                  VARCHAR(30)          not null,
ID_PERFIL            INT4                 not null,
TELEFONO             VARCHAR(30)          not null,
constraint PK_USUARIO primary key (DNI)
);

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
DNI
);

/*==============================================================*/
/* Index: POSEE_FK                                              */
/*==============================================================*/
create  index POSEE_FK on USUARIO (
ID_PERFIL
);

alter table RESERVA
   add constraint FK_RESERVA_HACE_USUARIO foreign key (USU_DNI)
      references USUARIO (DNI)
      on delete restrict on update restrict;

alter table RESERVA
   add constraint FK_RESERVA_OFRECE_SERVICIO foreign key (ID_SERVICIO)
      references SERVICIO (ID_SERVICIO)
      on delete restrict on update restrict;

alter table RESERVA
   add constraint FK_RESERVA_SE_USUARIO foreign key (DNI)
      references USUARIO (DNI)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_POSEE_PERFIL foreign key (ID_PERFIL)
      references PERFIL (ID_PERFIL)
      on delete restrict on update restrict;

