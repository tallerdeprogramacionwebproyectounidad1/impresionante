<?php

function conectarBD () {
	$host = "localhost";
	$db = "shop5";
	$user = "postgres"; 
	$password = "bryanahumada1";
	$port = "5432";

	// conectar a la base de datos
	try {
	    $conn = new PDO('pgsql:host='.$host.';port='.$port.';dbname='.$db.';user='.$user.';password='.$password);
	    return $conn;

	} catch (PDOException $e) {
	    echo $e->getMessage();
	    return false;
	}
}

/*
*/


function seleccionar($conn) {
    $sql = "select nombre, id_perfil from perfil;";
    $stmt = $conn->prepare($sql);
    try {
        if ($stmt->execute()) {
            // retorna valores como diccionario.
            $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
            echo "<p></p>";
            echo "<hr>";
            foreach ($data as $item) {
                echo $item["nombre"] . " " . $item["id_perfil"] . "<br>";
            }
        } else {
            $err = $stmt->errorInfo();
            echo $err[2] . "<br>";
            echo ("Error ejecutando: $sql <br>");
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function validarSesion() {
  if (isset($_SESSION["loggedinMIAPP"])) {
    if ($_SESSION["loggedinMIAPP"] != true) {
      logout();
    }
  } else {
    logout();
  }
}

function ejecutarSQL ($stmt) {
  $res = array();
  $res["success"] = false;
  $res["msg"] = "Error SQL";
  $res["data"] = null;
  
  try {
    if ($stmt->execute()) {
      $res["data"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $res["msg"] = "éxito";
      $res["success"] = true;
      
    } else {
      
      /* https://www.postgresql.org/docs/12/errcodes-appendix.html */
      $arrayError['23503'] = "El registro no se puede eliminar porque tiene información asociada.";
      
      $res["msg"] = "Error SQL, Contacte al soporte";      
    }

  } catch (PDOException $e) {
    $res["msg"] = $e->getMessage();
  }
  
  return $res;
}

?>
