$(function() { 
    obtener_Trabajadores();   
});

function obtener_Trabajadores() {
	
	var accion_agregar_trabajadores = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_trabajadores();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";        
    
	
	var tabla = $('#id_table5').dataTable({
		"columnDefs": [
          {"title": "Contraseña", "targets": 0, "orderable": true, "className": "dt-body-center"},
 		  {"title": "Nombre", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Rut", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Id_perfil", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": "telefono", "targets": 4, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar_trabajadores, "targets": 5, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "./language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	   $.ajax({
		url: 'bd/Trabajadores/Trabajadores.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.success) {

				var data = response.data;
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
						data[i]["password"],
					    data[i]["nombre"],
					    data[i]["dni"],
					    data[i]["id_perfil"],
					    data[i]["telefono"],

					    "<button type='button' class='btn btn-warning btn-xs' onclick='editar_3(" + data[i]["dni"] + ");' title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
					
					    "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar_3(" + data[i]["dni"] + ");' title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"

				    ]);
                }   
			}       
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function agregar_trabajadores() {
    $("#titulo-modal-Trabajadores").html("Agregar Trabajadores"); 
    document.getElementById("form-Trabajadores").reset();
    $("#btn-aceptar-Trabajadores").attr("onClick", "agregar_TrabajadoresBD()");
    $("#modal-Trabajadores").modal("show");
}

function agregar_TrabajadoresBD() {
    val_password = validarFormulario('#password', "password");
    val_nombre = validarFormulario('#nombre_1', "nombre_1");
    val_dni = validarFormulario('#dni', "dni");
    val_telefono = validarFormulario('#telefono_1', "telefono_1");

	if (val_password == false || val_nombre == false || val_dni == false || val_telefono == false){
         return false;
    }

    var password = $('#password').val();
	var nombre_1 = $('#nombre_1').val();
	var dni = $('#dni').val();
	var telefono_1 = $('#telefono_1').val();
    $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Trabajadores/Trabajadores.php?accion=2',
		data: {password: password,
                nombre_1: nombre_1,
                dni: dni,
            	telefono_1: telefono_1},

		success: function (response) {
			console.log(response.success); 
		    if (response.success) {          
			    $("#modal-Trabajadores").modal("hide");
			    obtener_Trabajadores();
			  
		    }

		},
    });
}

function validarFormulario (id, nombre) {
    if ($(id).val().trim().length<1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}

function eliminar_3(x){
	 $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Trabajadores/Trabajadores.php?accion=3',
		data: {x: x},


		success: function (response) {     
		    if (response.success) {
		    	obtener_Trabajadores();  
		    }
		},
    });
}

function editar_3(dni) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: 'bd/Trabajadores/Trabajadores.php?accion=4',
        data: {dni: dni},
        success: function (response) {
        	console.log(response.success);    
            if (response.success) {
                $.each(response.data, function(index, value) {
                    $("#password").val(response.data["password"]);
                    $('#nombre_1').val(response.data["nombre"]);
                    $('#dni').val(response.data["dni"]);
                    $('#telefono_1').val(response.data["telefono"]);
                });
    
                $("#titulo-modal-Trabajadores").html("Editar");
                $("#btn-aceptar-Trabajadores").attr('onclick','editar_3BD('+dni+');');
                $("#modal-Trabajadores").modal("show");
            }   
        },
    }); 
}

function editar_3BD(dni) {
    var password = $('#password').val();
	var nombre_1 = $('#nombre_1').val();
	var rut = $('#dni').val();
	var telefono_1 = $('#telefono_1').val();
    

    $.ajax({
        dataType: 'json',
        async: true,
        url: 'bd/Trabajadores/Trabajadores.php?accion=5',
		data: {dni: dni,
			   password: password, 
               nombre_1: nombre_1, 
               rut: rut,
           	   telefono_1: telefono_1},
                    
        success: function (response) {  
            if (response.success) {          
                $("#modal-Trabajadores").modal("hide");
                obtener_sucursales();
            } 
        }, 
    });
}