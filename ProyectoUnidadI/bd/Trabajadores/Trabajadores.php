<?php

require_once("../conexionBd.php");

session_start();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar_1($conn);
      break;
    case 2:
      # insertar
      insertar($conn);
      break;
    case 3:
      # delete
      eliminar($conn);
      break;
    case 4:
      # update
      seleccionarUno($conn);
      break;
    case 5:
      # update
      actualizar($conn);
      break;         
  }  
}

function seleccionar_1($conn) {
  $sql= "select * from usuario where id_perfil = 2;";
  
  $stmt = $conn->prepare($sql);
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function insertar($conn) {
  $password = trim($_REQUEST['password']);
  $nombre_1 = trim($_REQUEST['nombre_1']);
  $dni = trim($_REQUEST['dni']);
  $telefono_1 = trim($_REQUEST['telefono_1']);

  $sql = "insert into usuario(password, nombre, dni, id_perfil, telefono) values (:password, :nombre_1, :dni, 2, :telefono_1);";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':password', $password);
  $stmt->bindValue(':nombre_1', $nombre_1);
  $stmt->bindValue(':dni', $dni);
  $stmt->bindValue(':telefono_1', $telefono_1);

  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function eliminar($conn) {
  $dni = $_REQUEST['x'];

  $sql = "delete from usuario where dni = :dni;";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':dni', $dni);
  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

function seleccionarUno ($conn) {
  $dni = $_REQUEST['dni'];
  
  $sql = "select password, nombre, dni, telefono from usuario where dni = :dni;";
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':dni', $dni);

  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"][0]));
}

function actualizar ($conn) {
  $dni = trim($_REQUEST['dni']);
  $password = trim($_REQUEST['password']);
  $nombre_1 = trim($_REQUEST['nombre_1']);
  $rut = trim($_REQUEST['rut']);
  $telefono_1 = trim($_REQUEST['telefono_1']);
 

  $sql = "update usuario set password = :password, nombre = :nombre_1, dni = :rut, telefono = :telefono_1 where dni = :dni;";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':password', $password);
  $stmt->bindValue(':nombre_1', $nombre_1);
  $stmt->bindValue(':rut', $rut);
  $stmt->bindValue(':telefono_1', $telefono_1);
  $stmt->bindValue(':dni', $dni);
  
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}


?>

