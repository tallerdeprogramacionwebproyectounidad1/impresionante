<?php

require_once("../conexionBd.php");

session_start();

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar_2($conn);
      break;
    case 2:
      # delete
      eliminar_2($conn);
    break;
  }
}

function seleccionar_2($conn) {
  $sql= "select * from reserva;";
  
  $stmt = $conn->prepare($sql);
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"]));
}

function eliminar_2($conn) {
  $id_reserva = $_REQUEST['x'];

  $sql = "delete from reserva where id_reserva = :id_reserva;";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_reserva', $id_reserva);
  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("success"=>$res["success"], "msg"=>$res["msg"], "data"=>$res["data"])); 
}

?>