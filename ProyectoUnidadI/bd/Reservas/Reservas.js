$(function() { 
    obtener_Reservas();   
});

function obtener_Reservas() {
	    
	var tabla = $('#id_table4').dataTable({
		"columnDefs": [
          {"title": "Id_Reserva", "targets": 0, "orderable": true, "className": "dt-body-center"},
 		  {"title": "Rut", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Id_Servicio", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Rut Trabajador", "targets": 3, "orderable": false, "className": "dt-nowrap dt-right"},
		  {"title": "botones", "targets": 4, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "./language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	   $.ajax({
		url: 'bd/Reservas/Reservas.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.success) {
				var data = response.data;
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
						data[i]["id_reserva"],
					    data[i]["dni"],
					    data[i]["id_servicio"],
					    data[i]["usu_dni"],
					
					    "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar_2(" + data[i]["id_reserva"] + ");' title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"

				    ]);
                }   
			}       
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function eliminar_2(x){
	 $.ajax({
		dataType: 'json',
		async: true,
		url: 'bd/Reservas/Reservas.php?accion=2',
		data: {x: x},


		success: function (response) {     
		    if (response.success) {
		    	obtener_Reservas();  
		    }
		},
    });
}
