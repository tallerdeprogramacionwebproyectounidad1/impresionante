<?php

setlocale (LC_TIME, "es_ES.utf8");

function validarSesion() {
  if (isset($_SESSION["loggedinMIAPP"])) {
    if ($_SESSION["loggedinMIAPP"] != true) {
      logout();
    }
  } else {
    logout();
  }
}

function validarSesionTrabajador() {
  if (isset($_SESSION["loggedintrabajador"])) {
    if ($_SESSION["loggedintrabajador"] != true) {
      logout();
    }
  } else {
    logout();
  }
}

function validarSesionCliente() {
  if (isset($_SESSION["loggedinusuario"])) {
    if ($_SESSION["loggedinusuario"] != true) {
      logout();
    }
  } else {
    logout();
  }
}

function logout() {
  header('Location: uwu.php');
}

function modales($x){
  if ($x == "iniciar"){
    $str = <<<EOF
      <div class="card bg-light">
        <div class="login-main-text">
          <article class="card-body mx-auto" style="max-width: 400px;">
            <h2>iniciar sesión</h2>
        </div>
      </div>
      <div class="main">
        <div class="col-md-6 col-sm-12">
          <div class="login-form">
            <form id="login_form" name="login_form" class="form-horizontal" role="form" method="POST">
              <div class="form-group">
                <label>RUT</label>
                <input id="correo" type="text" class="form-control" name="correo" placeholder="RUT">
              </div>
              <div class="form-group">
                <label>Contraseña</label>
                <input id="clave" type="password" class="form-control" name="clave" placeholder="Contraseña">
              </div>
              <label id="error"></label><br>
               <button id="submit_login" type="submit" class="btn btn-success btn-sm btn-block" style="background-color: #000">Iniciar Sesión</button>
            </form>
          </div>
      </div>
            </article>
          </div>
    EOF;
  }

  if ($x == "Registrarse"){
    $str = <<<EOF
      <div class="card bg-light">
        <article class="card-body mx-auto" style="max-width: 400px;">
          <h4 class="card-title mt-3 text-center">Crear Cuenta</h4>
          <form id="register_form" name="register_form" class="form-horizontal" role="form" method="post">
            
            <div class="form-group input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-user"></i> </span>
              </div>
                  <input name="nombre" id="nombre" class="form-control" placeholder="Nombre Completo" type="text">
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
              </div>
                  <input name="rut" id="rut" class="form-control" placeholder="RUT" type="text">
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
              </div>
              <select class="custom-select" style="max-width: 120px;">
                  <option selected="">+569</option>
              </select>
                <input name="telefono" id="telefono" class="form-control" placeholder="Número Telefónico" type="text">
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
              </div>
                  <input name="clave1" id="clave1" class="form-control" placeholder="Crear Contraseña" type="password">
              </div> <!-- form-group// -->
              <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
              </div>
                  <input name="clave2" id="clave2" class="form-control" placeholder="Repetir Contraseña" type="password">
              </div> <!-- form-group// -->                                      
              <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block" style="background-color: #000"> Crear Cuenta  </button>
              </div> <!-- form-group// -->      
              <p class="text-center">¿Tienes una cuenta? <a href="" style="color: #c13584">Iniciar Sesión</a> </p>                                                               
          </form>
        </article>
      </div> <!-- card.// -->
    EOF;
  }
  echo $str;
}


function Logo(){
  $str = <<<EOF
    <div class="logo">
      <h1><i class="fi-rr-scissors"></i> La Barbière de Paris <i class="fi-rr-scissors"></i></h1>
      <img src="imagenes/barberia3.jpg" width="250" />
      <p style="color:#FFFFFF" >Barbershop - Talca</p>
    </div>
    <main>
      <nav>
        <a  href="#" class="banner" data-pushbar-target="pushbar-productos">Talento Exclusivo</a>
      </nav>
    </main>
  EOF;
  echo $str;
}


function head(){
  $str = <<<EOF
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/pushbar.css">
    <link rel="icon" href="imagenes/barberia3.jpg">
    <link rel="stylesheet" href="css/estilos.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
    <!-- Funciones para el click -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans" rel="stylesheet" type="text/css">   
    <!-- bootsss -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <!-- carga archivos javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js" integrity="sha256-JG6hsuMjFnQ2spWq0UiaDRJBaarzhFbUxiUTxQDA9Lk=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js" integrity="sha256-XF29CBwU1MWLaGEnsELogU6Y6rcc5nCkhhx89nFMIDQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js" integrity="sha256-J2sc79NPV/osLcIpzL3K8uJyAD7T5gaEFKlLDM18oxY=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" integrity="sha256-CfcERD4Ov4+lKbWbYqXD6aFM9M51gN4GUEtDhkWABMo=" crossorigin="anonymous"></script>

    <!-- Base de datos flaction -->
    <link href="css/uicons-regular-rounded.css" rel="stylesheet">
    <!-- Contenido del CSS -->
    <link rel="stylesheet" type="text/css" href="css/lindi.css">
    <link rel="stylesheet" type="text/css" href="css/noticias.css">
    <link rel="stylesheet" type="text/css" href="css/contacto.css">   
    <!-- script que agrega los iconos a las redes sociales -->
    <script src="https://kit.fontawesome.com/2c36e9b7b1.js"></script>
    <title>La Barbière de Paris</title>
  EOF;
  echo $str;
}

function Noticias(){
  $str = <<<EOF
  <div class="container">
          <div class="container">
          <h2 class="pb-3 pt-2 border-bottom mb-5">Últimas Noticias</h2>
          <!--first section-->
          <div class="row align-items-center how-it-works d-flex">
              <div class="col-2 text-center bottom d-inline-flex justify-content-center align-items-center">
                <div class="circle font-weight-bold">1</div>
            </div>
              <div class="col-6">
                <h5>Fully Responsive</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p>
              </div>
          </div>
          <!--path between 1-2-->
          <div class="row timeline">
              <div class="col-2">
                <div class="corner top-right"></div>
              </div>
              <div class="col-8">
                <hr/>
              </div>
              <div class="col-2">
                <div class="corner left-bottom"></div>
            </div>
          </div>
          <!--second section-->
          <div class="row align-items-center justify-content-end how-it-works d-flex">
              <div class="col-6 text-right">
                <h5>Using Bootstrap</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p>
              </div>
              <div class="col-2 text-center full d-inline-flex justify-content-center align-items-center">
                <div class="circle font-weight-bold">2</div>
              </div>
          </div>
          <!--path between 2-3-->
          <div class="row timeline">
              <div class="col-2">
                <div class="corner right-bottom"></div>
              </div>
              <div class="col-8">
                <hr/>
              </div>
              <div class="col-2">
                <div class="corner top-left"></div>
              </div>
          </div>
          <!--third section-->
          <div class="row align-items-center how-it-works d-flex">
              <div class="col-2 text-center top d-inline-flex justify-content-center align-items-center">
                <div class="circle font-weight-bold">3</div>
              </div>
              <div class="col-6">
                <h5>Now with Pug and Sass</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor gravida aliquam. Morbi orci urna, iaculis in ligula et, posuere interdum lectus.</p>
              </div>
          </div>
        </div>
  EOF;

  echo $str;
}

function Contacto(){
  $str = <<<EOF
    <ul class="thumbnails">                         
          <li class="span3">
            <div class="product-box">
              <span class="sale_tag"></span>
              <a class="title">Carlos Figueroa</a><br/>
              <a class="category">Dueño</a>
              <p class="category">+569 3351 5135</p>
            </div>
          </li>
          <li class="span3">
            <div class="product-box">
              <a class="title">Horario De Atencion</a><br/>
              <a class="category">Lunes-Viernes: 9:00 am a 17:00 pm</a>
            </div>
          </li>
          <li class="span3">
            <div class="product-box">
              <a class="title">Conoce a Nuestros Trabajadores</a><br/>
              <a class="category">buttom</a>
            </div>
          </li>
          <li class="span3">
            <div class="product-box">
              <span class="sale_tag"></span>
              <a class="title">Encuentranos</a><br/>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2733.255597382585!2d-71.69460426285582!3d-35.44471704042087!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1ddce1ee93046c1d!2sFlorida%20Barbershop!5e0!3m2!1ses!2scl!4v1621220677549!5m2!1ses!2scl" width="1000" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
          </li>
        </ul> 
  EOF;
  echo $str;
}

function Inicio(){
  $str = <<<EOF
    <img src="imagenes/foto_inicio.jpg"/>
        <img src="imagenes/covid.jpg" style="margin-top: -1400px"/>
        <div class="row">
            <div class="col-md-4">
              <a href="imagenes/barberia3.jpg" class="thumbnail">
                <p>Pulpit Rock: A famous tourist attraction in Forsand, Ryfylke, Norway.</p>
                <img src="imagenes/barberia3.jpg" alt="Pulpit Rock" style="width:150px;height:150px">
              </a>
            </div>
            <div class="col-md-4">
              <a href="imagenes/foto_inicio.jpg" class="thumbnail">
                <p>Moustiers-Sainte-Marie: Considered as one of the "most beautiful villages of France".</p>
                <img src="imagenes/foto_inicio.jpg" alt="Moustiers Sainte Marie" style="width:150px;height:150px">
              </a>
            </div>
            <div class="col-md-4">
              <a href="imagenes/barberia2.png" class="thumbnail">
                <p>The Cinque Terre: A rugged portion of coast in the Liguria region of Italy.</p>
                <img src="imagenes/barberia2.png" alt="Cinque Terre" style="width:150px;height:150px">
              </a>
            </div>
  EOF;
  echo $str;
}

function Reservas(){
  $str = <<<EOF
    <div class="container">
          <div class="titulo"> Nuestro Servicios de La Barbería </div>
          <br>
          <br>
              <div class="card-deck" id="carta" name="carta">
                <div class="card">
                    <img class="card-img-top img-fluid" src="imagenes/fondo.jpg">
                    <div class="text-center">
                      <img src="imagenes/servicios/afeitado.png" alt="" class="img-fluid yoimagen rounded-circle" height="150px" width="150px">
                    </div>
                    <div class="card-block">
                      <h4 class="card-title" id="titulo" name="titulo" style="background-color: #000;">Card title</h4>
                  </div>
                    <div class="card-footer text-center">
                        <a href="#" data-toggle="modal" data-target="#miModal3" class="btn btn-dark">Reservar ahora</a>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top img-fluid" src="imagenes/fondo.jpg" alt="Card image cap">
                    <div class="text-center">
                      <img src="imagenes/servicios/afeitado_barba" alt="" class="img-fluid yoimagen rounded-circle" height="150px" width="150px">
                    </div>
                    <div class="card-block">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card-footer text-center">
                      <a href="#" class="btn btn-dark">Reservar ahora</a>
                    </div>
                </div>
                <div class="card">
                    <img class="card-img-top img-fluid" src="imagenes/fondo.jpg" alt="Card image cap">
                    <div class="text-center">
                    <img src="imagenes/servicios/corte_afeitado.png" alt="" class="img-fluid yoimagen rounded-circle" height="150px" width="150px">
                    </div>
                    <div class="card-block">
                      <h4 class="card-title">Card title</h4>
                      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card-footer text-center">
                      <a href="#" class="btn btn-dark">Reservar ahora</a>
                    </div>
                </div>
             </div>
          </div>
        
  EOF;
  echo $str;
}

function footer(){
  $str = <<<EOF
  <div class="redes-sociales">
      <div class="contenedor-icono">
        <a href="http://www.twitter.com" target="_blank" class="twitter" >
          <i class="fab fa-twitter"></i>
        </a>
      </div>
      <div class="contenedor-icono">
        <a href="http://www.facebook.com" target="_blank" class="facebook" >
          <i class="fab fa-facebook-f"></i>
        </a>
      </div>
      <div class="contenedor-icono">
        <a href="http://www.instagram.com" target="_blank" class="instagram">
          <i class="fab fa-instagram"></i>
        </a>
      </div>
    </div>

    <div class="creado-por">
      <p>Sitio diseñado por <a href="#" style="color:#FFFFFF"> Benjamín Astudillo & Bryan Ahumada  </a> - <a style="color:#FFFFFF;">La Barbière de Paris-</a></p>
    </div>
    <script src="extras/modernizr/modernizr-2.8.3.min.js"></script>
    <script src="extras/jquery/jquery-3.2.1.min.js"></script>
    <script src="extras/bootstrap/bootstrap.min.js"></script>
    <script src="extras/sweetalert2/sweetalert2.min.js"></script>
    <script src="extras/jquery/jquery-ui.min.js"></script>
    <script src="controladores/common.js"></script>
  EOF;

  echo $str;
}



function image($x){
  if ($x == 1){
    $str = <<<EOF
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-door" viewBox="0 0 16 16">
        <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
      </svg>
    EOF;
  }
  if ($x == 2){
    $str = <<<EOF
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hand-index-thumb" viewBox="0 0 16 16">
      <path d="M6.75 1a.75.75 0 0 1 .75.75V8a.5.5 0 0 0 1 0V5.467l.086-.004c.317-.012.637-.008.816.027.134.027.294.096.448.182.077.042.15.147.15.314V8a.5.5 0 0 0 1 0V6.435l.106-.01c.316-.024.584-.01.708.04.118.046.3.207.486.43.081.096.15.19.2.259V8.5a.5.5 0 1 0 1 0v-1h.342a1 1 0 0 1 .995 1.1l-.271 2.715a2.5 2.5 0 0 1-.317.991l-1.395 2.442a.5.5 0 0 1-.434.252H6.118a.5.5 0 0 1-.447-.276l-1.232-2.465-2.512-4.185a.517.517 0 0 1 .809-.631l2.41 2.41A.5.5 0 0 0 6 9.5V1.75A.75.75 0 0 1 6.75 1zM8.5 4.466V1.75a1.75 1.75 0 1 0-3.5 0v6.543L3.443 6.736A1.517 1.517 0 0 0 1.07 8.588l2.491 4.153 1.215 2.43A1.5 1.5 0 0 0 6.118 16h6.302a1.5 1.5 0 0 0 1.302-.756l1.395-2.441a3.5 3.5 0 0 0 .444-1.389l.271-2.715a2 2 0 0 0-1.99-2.199h-.581a5.114 5.114 0 0 0-.195-.248c-.191-.229-.51-.568-.88-.716-.364-.146-.846-.132-1.158-.108l-.132.012a1.26 1.26 0 0 0-.56-.642 2.632 2.632 0 0 0-.738-.288c-.31-.062-.739-.058-1.05-.046l-.048.002zm2.094 2.025z"/>
      </svg>
    EOF;    
  }
  if ($x == 3){
    $str = <<<EOF
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-inbound" viewBox="0 0 16 16">
      <path d="M15.854.146a.5.5 0 0 1 0 .708L11.707 5H14.5a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793L15.146.146a.5.5 0 0 1 .708 0zm-12.2 1.182a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
      </svg>
    EOF;
    
  }
  if ($x == 4){
    $str = <<<EOF
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pin-angle" viewBox="0 0 16 16">
        <path d="M9.828.722a.5.5 0 0 1 .354.146l4.95 4.95a.5.5 0 0 1 0 .707c-.48.48-1.072.588-1.503.588-.177 0-.335-.018-.46-.039l-3.134 3.134a5.927 5.927 0 0 1 .16 1.013c.046.702-.032 1.687-.72 2.375a.5.5 0 0 1-.707 0l-2.829-2.828-3.182 3.182c-.195.195-1.219.902-1.414.707-.195-.195.512-1.22.707-1.414l3.182-3.182-2.828-2.829a.5.5 0 0 1 0-.707c.688-.688 1.673-.767 2.375-.72a5.922 5.922 0 0 1 1.013.16l3.134-3.133a2.772 2.772 0 0 1-.04-.461c0-.43.108-1.022.589-1.503a.5.5 0 0 1 .353-.146zm.122 2.112v-.002.002zm0-.002v.002a.5.5 0 0 1-.122.51L6.293 6.878a.5.5 0 0 1-.511.12H5.78l-.014-.004a4.507 4.507 0 0 0-.288-.076 4.922 4.922 0 0 0-.765-.116c-.422-.028-.836.008-1.175.15l5.51 5.509c.141-.34.177-.753.149-1.175a4.924 4.924 0 0 0-.192-1.054l-.004-.013v-.001a.5.5 0 0 1 .12-.512l3.536-3.535a.5.5 0 0 1 .532-.115l.096.022c.087.017.208.034.344.034.114 0 .23-.011.343-.04L9.927 2.028c-.029.113-.04.23-.04.343a1.779 1.779 0 0 0 .062.46z"/>
      </svg> 
    EOF;
  }
  if ($x == 5){
    $str = <<<EOF
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bar-chart-line" viewBox="0 0 16 16"><path d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2zm1 12h2V2h-2v12zm-3 0V7H7v7h2zm-5 0v-3H2v3h2z"/></svg>
    EOF;
  }
  echo $str;
}

function Gestionar_Noticias(){
  
  $str = <<<EOF
          <input type='submit' onclick='btnMinformacion();'  class='btn btn-success' value='Añadir'>
          <div id="mas"></div>
          <button type="button" class="btn btn-default">Editar</button>
          <button type="button" class="btn btn-default">Eliminar</button>
        </div>
      </div>
  EOF;
  echo $str;
}

  

?>