// Shorthand for $( document ).ready()
$(function() {
  
  $("#register_form").submit(function () {
    var user_name = $('#nombre').val().trim();
    var user_rut = $('#rut').val().trim();
    var user_telefono = $('#telefono').val().trim();
    var user_clave1 = $('#clave1').val().trim();
    var user_clave2 = $('#clave2').val().trim();
    
    $.ajax({
      type: "POST",
      dataType: 'json',
      url: "bd/register.php",
      data: {nombre: user_name, rut: user_rut, telefono: user_telefono, clave1: user_clave1, clave2: user_clave2},
      async: "false",
      success: function (response) {
        if (response.success) {
          window.location=response.location;
        } else {
          showMessageAlert(response.msg);
        }
      }, /*error: function (e) {
        logout();
      }*/
      });
    
    $('#nombre').val('');
    $('#rut').val('');
    $('#telefono').val('');
    $('#clave1').val('');
    $('#clave2').val('');
    return false;
  });
});
