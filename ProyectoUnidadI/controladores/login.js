// Shorthand for $( document ).ready()
$(function() {
  
  $("#login_form").submit(function () {
    var user_name = $('#correo').val().trim();
    var user_password = $('#clave').val().trim();
    
    $.ajax({
      type: "POST",
      dataType: 'json',
      url: "./bd/check_login.php",
      data: {correo: user_name, clave: user_password},
      async: "false",
      success: function (response) {
        if (response.success) {
          window.location=response.location;
        } else {
          showMessageAlert(response.msg);
        }
      }, /*error: function (e) {
        logout();
      }*/
      });
    
    $('#username').val('');
    $('#password').val('');
    return false;
  });
});
